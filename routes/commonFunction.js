/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var fs = require('fs');
var AWS = require('aws-sdk');
var generatePassword = require('password-generator');

exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}


exports.uploadImage = function (file, res, folder, callback) {
    var filename = file.name;
    var path = file.path;
    var mimeType = file.type;

    fs.readFile(path, function (error, file_buffer) {
        if (error) {
            sendResponse.somethingWentWrongError(res);
        }
        filename = file.name;
        finalpath = config.get("s3").Url + filename;
        srcpath = config.get("s3").Url;
        if (finalpath == srcpath) {
            sendResponse.emptyFileUpload(res);
        }
        else {
            AWS.config.update({accessKeyId: config.get("s3").AccessKey, secretAccessKey: config.get("s3").SecretKey});
            var s3bucket = new AWS.S3();
            var params = {
                Bucket: config.get("s3").userName,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ACL: 'public-read',
                ContentType: 'image/jpeg'
            };
            console.log(params);
            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    return callback(finalpath);
                }
            });
        }
    });
};


exports.checkEmailAvailability = function (res, sql, email, callback) {


    connection.query(sql, [email], function (err, response) {

        if (err) {
            console.log(err);
            console.log("email");
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            sendResponse.emailExists(res);
        }
        else {
            callback();
        }
    });
};
exports.checkEmailExist = function (res, sql, email, callback) {


    connection.query(sql, [email], function (err, response) {

        if (err) {
            console.log(err);
            console.log("email");
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            callback();
        }
        else {
            sendResponse.emailNotRegistered(res);
        }
    });
};

exports.checkEmail = function (res, sql, email, accessToken, callback) {


    connection.query(sql, [email], function (err, response) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            if (response[0].access_token === accessToken) {
                callback();
            }
            else {
                sendResponse.emailExists(res);
            }

        }
        else {
            callback();

        }
    });
};

exports.generateRandomString = function () {
    var math = require('math');

    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};
function generateRandomString() {
    var math = require('math');

    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(math.floor(math.random() * possible.length));

    return text;
};

exports.sendMailTo = function (userName, email, res) {

    var toEmail = email;
    var sub = "Welcome!!";
    var html = '<html>';
    html += '<head>';
    html += '<title></title>';
    html += '</head>';
    html += '<body>';
    html += 'Welcome';
    html += '<p>Dear ' + userName + ',</p>';
    html += '<p>Hello.</p>';
    html += '<p>Please do not reply to this email. Emails sent to this address will not be answered.</p>';
    html += '<br/>';
    html += '<span>Copyright &copy; 2015 Jashanpreet Singh, LLC. All rights reserved.</span>';
    html += '</body>';
    html += '</html>';
    sendEmail(toEmail, html, sub, function (result) {
        if (result === 1) {
            console.log("Mail Sent");

        }
        else {
            console.log("Error Sending Mail");
        }
    });
}

function sendEmail(receiverMailId, message, subject, callback) {

    var nodemailer = require('nodemailer');
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message
        //html: "<b>Hello world ?</b>" // html body
    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {

        if (error) {
            return callback(0);

        } else {
            //  console.log('hi');
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });
};


exports.emailLogin = function (email, sql, user_status, password, res) {
    console.log(checkBlank);
    connection.query(sql, [email], function (err, result) {

        if (err) {

            sendResponse.somethingWentWrongError(res);
        }
        else if (result.length == 0) {
            sendResponse.emailNotRegistered(res);
        }
        else if (result[0].login_status === 1) {
            sendResponse.alreadyLoggedIn(res);
        }
        else {
            if (user_status === 1) {
                tb = config.get("tb_name").user;
            }
            else {
                tb = config.get("tb_name").driver;
            }
            var md5 = require('MD5');
            var hash = md5(password);

            var sql = "SELECT `access_token`,`first_name`,`phone_no`,`device_token`,`device_type`,`app_version`,`lng`,`lat` FROM " + tb + " WHERE `email`=? && `password`=? LIMIT 1"

            connection.query(sql, [email, hash], function (err, result_user) {
                if (err) {

                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }
                else {

                    var result_user_length = result_user.length;
                    if (result_user_length === 0) {
                        sendResponse.passwordIncorrect(res);

                    }
                    else {
                        var loginTime = new Date();
                        var accesstoken = generateRandomString();
                        var accesstoken1 = accesstoken + email;
                        var accessToken = md5(accesstoken1);
                        var sql = 'UPDATE ' + tb + '  SET `last_login`=? ,`login_status`=? ,`access_token` = ?  WHERE `email`=? LIMIT 1';
                        connection.query(sql, [loginTime, 1, accessToken, email], function (err, result) {
                            if (err) {
                                console.log(err);
                                console.log("hey");

                                sendResponse.somethingWentWrongError(res);
                            }

                            else {

                                sendResponse.sendSuccessReport(result_user, res);
                            }
                        });


                    }

                }

            });
        }

    });


}


exports.accessTokenLogin = function (accessToken, sql, user_status, res) {
    connection.query(sql, [accessToken], function (err, result) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (result.length == 0) {
            sendResponse.invalidAccessToken(res);
        }
        else {
            if (user_status === 1) {
                var sql = "UPDATE `tb_user` SET `last_login`=? ,`login_status`=? ,`access_token` = ?  WHERE `access_token`=? LIMIT 1";
            }
            else {
                var sql = "UPDATE `tb_driver` SET `last_login`=? ,`login_status`=? ,`access_token` = ?  WHERE `access_token`=? LIMIT 1";

            }
            var loginTime = new Date();
            var accesstoken = generateRandomString();
            var accesstoken1 = accesstoken + result[0].email;
            var accessToken1 = md5(accesstoken1);

            connection.query(sql, [loginTime, 1, accessToken1, accessToken], function (err, result1) {
                if (err) {

                    sendResponse.somethingWentWrongError(res);
                }

                else {
                    sendResponse.sendSuccessReport(result, res);
                }
            });

        }


    });
}

exports.sendMailToResetPassword = function (userName, user_status, email, res) {
    var password = generatePassword(6, false);
    var encrypted_password = md5(password);
    var toEmail = email;
    var sub = "Password Reset";
    var html = "Information to reset your password";
    html += "Dear ' + userName + '";
    html += "Thank you for contacting us. Click the button below to reset your password. This link is valid for one use only.";
    html += "'+encrypted_password+'";
    html += 'Please do not reply to this email. Emails sent to this address will not be answered.';
    html += 'Copyright &copy; 2015 Jashanpreet Singh, LLC. All rights reserved.';
    sendEmail(toEmail, html, sub, function (result) {
        if (result == 1) {
            if (user_status === 1) {
                tb = config.get("tb_name").user;
            }
            else {
                tb = config.get("tb_name").driver;
            }
            var sql = "UPDATE " + tb + " SET `temporary_link`=?   WHERE `email`=? LIMIT 1"
            connection.query(sql, [encrypted_password, toEmail], function (err, result) {
                if (err) {

                    sendResponse.somethingWentWrongError(err);
                }
                else {

                    sendResponse.sendLog(res);
                }
            })

        }
        else {
            console.log("hello");
            sendResponse.somethingWentWrongError(res);
        }
    });
}

