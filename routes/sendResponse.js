/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse, res);
};

exports.emailNotRegistered = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.EMAIL_NOT_REGISTERED,
        data: {}
    }
    sendData(errResponse, res);
};

exports.noDriver = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.NO_DRIVER,
        data: {}
    }
    sendData(errResponse, res);
};

exports.notVerified = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.NOT_VERIFIED,
        data: {}
    }
    sendData(errResponse, res);
};

exports.passwordIncorrect = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.PASSWORD_INCORRECT,
        data: {}
    }
    sendData(errResponse, res);
};

exports.emailExists = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.EMAIL_EXISTS,
        data: {}
    }
    sendData(errResponse, res);
};

exports.alreadyLoggedIn = function (res) {

    var errResponse = {
        status: constant.responseStatus.ACCESS_TOKEN_ERROR,
        message: constant.responseMessage.LOGGED_IN,
        data: {}
    }
    sendData(errResponse, res);
};

exports.sendError = function (msg, res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: msg,
        data: {}
    };
    sendData(errResponse, res);
};
exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse, res);
};

exports.invalidAccessToken = function (res) {

    var errResponse = {
        status: constant.responseStatus.ACCESS_TOKEN_ERROR,
        message: constant.responseMessage.INVALID_ACCESS_TOKEN,
        data: {}
    }
    sendData(errResponse, res);
};

exports.noDriverAvailable = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.NO_DRIVER_AVAILABLE,
        data: {}
    }
    sendData(errResponse, res);
};
exports.driverAlreadyAssigned = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.DRIVER_ASSIGNED,
        data: {}
    }
    sendData(errResponse, res);
};
exports.invalidRide = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.INVALID_RIDE,
        data: {}
    }
    sendData(errResponse, res);
};
exports.sendSuccessData = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse, res);
};

exports.sendDriverData = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.DRIVER_SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse, res);
};

exports.editSuccessful = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.EDIT_SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse, res);
};
exports.sendSuccessLogout = function (res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.SUCCESSFUL_LOGOUT,
        data: {}
    };
    sendData(successResponse, res);
};
exports.sendSuccessReport = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.SUCCESSFUL_LOGGING,
        data: data
    };
    sendData(successResponse, res);
};
exports.rideStarted = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.RIDE_STARTED,
        data: data
    };
    sendData(successResponse, res);
};
exports.rideEnded = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.RIDE_ENDED,
        data: data
    };
    sendData(successResponse, res);
};
exports.driverSuccessfullyAssigned = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.SUCCESSFULLY_ASSIGNED,
        data: data
    };
    sendData(successResponse, res);
};

exports.sendLog = function (res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.RESET_EMAIL_SEND,
        data: {}
    }
    sendData(successResponse, res);
};


exports.wrongLink = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.WRONG_LINK,
        data: {}
    }
    sendData(errResponse, res);
};


exports.resetSuccessful = function (res) {

    var successResponse = {
        status: constant.responseStatus.SUCCESS,
        message: constant.responseMessage.RESET_SUCCESSFUL,
        data: {}
    }
    sendData(successResponse, res);
};

exports.sendData = function (data, res) {
    sendData(data, res);
};


function sendData(data, res) {
    res.type('json');
    res.jsonp(data);
}