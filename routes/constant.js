/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "ERROR_IN_EXECUTION", 100);
define(exports.responseStatus, "SUCCESS", 101);
define(exports.responseStatus, "ACCESS_TOKEN_ERROR", 102);
define(exports.responseStatus, "NEW_REQUEST", 103);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "UPDATE_APP", "Update App Version.");
define(exports.responseMessage, "LOGGED_IN", "You are already logged in.");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "SHOW_MESSAGE", "Hi there!");
define(exports.responseMessage, "SHOW_DATA", "");
define(exports.responseMessage, "EMAIL_EXISTS", "this email is already registered");
define(exports.responseMessage, "SUCCESSFUL_EXECUTION", "Registration Successful.");
define(exports.responseMessage, "DRIVER_SUCCESSFUL_EXECUTION", "Event Successful.");
define(exports.responseMessage, "SUCCESSFUL_LOGGING", "Login Successful.");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered. Please sign up.");
define(exports.responseMessage, "PASSWORD_INCORRECT", "Sorry, your password is incorrect");
define(exports.responseMessage, "SUCCESSFUL_LOGOUT", "Logout Successful.");
define(exports.responseMessage, "INVALID_ACCESS_TOKEN", "Invalid Access Token.");
define(exports.responseMessage, "EDIT_SUCCESSFUL_EXECUTION", "Edit Successful.");
define(exports.responseMessage, "RESET_SUCCESSFUL", "Reset Successful.");
define(exports.responseMessage, "RESET_EMAIL_SEND", "Check your email to change password");
define(exports.responseMessage, "WRONG_LINK", "Link does not exists or has already been used.");
define(exports.responseMessage, "NOT_VERIFIED", "The driver has not yet verified.");
define(exports.responseMessage, "NO_DRIVER", "No driver Registered.");
define(exports.responseMessage, "NO_DRIVER_AVAILABLE", "No driver Available at the moment.Please try after sometime.");
define(exports.responseMessage, "NEW_REQUEST", "New Ride Alert!");
define(exports.responseMessage, "DRIVER_ASSIGNED", "Driver Already Assigned.");
define(exports.responseMessage, "SUCCESSFULLY_ASSIGNED", "Driver Successfully Assigned.");
define(exports.responseMessage, "RIDE_STARTED", "Ride has started.");
define(exports.responseMessage, "INVALID_RIDE", "Invalid Ride.");
define(exports.responseMessage, "RIDE_ENDED", "Ride has ended.");

