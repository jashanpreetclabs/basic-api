var router = express.Router();
var async = require('async');
var geolib = require('geolib');


/* Sign_Up Panel */

router.post('/sign_up', function (req, res, next) {
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var phoneNo = req.body.phone_no;
    var email = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var lat = req.body.lat;
    var lng = req.body.lng;
    var checkBlank = [firstName, lastName, phoneNo, email, password, deviceType, deviceToken, appVersion, lat, lng];
    console.log(checkBlank);
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);

        },
        function (callback) {
            var sql = "SELECT `user_id` FROM `tb_user` WHERE `email`=? limit 1";
            func.checkEmailAvailability(res, sql, email, callback);

        }], function (updatePopup) {

        var hash = md5(password);
        var accesstoken = func.generateRandomString();
        var accesstoken1 = accesstoken + email;
        var accessToken = md5(accesstoken1);
        var loginTime = new Date();
        func.uploadImage(req.files.user_image, res, 'jashan', function (result) {
            var sql = "INSERT into tb_user(email,password,access_token,date_registered,last_login,first_name,last_name,image,phone_no,device_type,device_token,app_version,lat,lng) \n\
                    values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            connection.query(sql, [email, hash, accessToken, loginTime, loginTime, firstName, lastName, result, phoneNo, deviceType, deviceToken, appVersion, lat, lng], function (err, result) {

                if (err) {
                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }

                else {
                    var sql = "SELECT `first_name`,`last_name`,`access_token`,`phone_no`,`user_id` ,`device_type`,`device_token`,`app_version`,`lat`,`lng` FROM \n\
                       `tb_user` WHERE `email`=? LIMIT 1";
                    connection.query(sql, [email], function (err, user) {
                        if (err) {
                            console.log("hey");
                            console.log(err);
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var final = {
                                "first_name": user[0].first_name,
                                "last_name": user[0].last_name,
                                "access_token": user[0].access_token,
                                "email": email,
                                "phone_no": user[0].phone_no,
                                "device_type": user[0].device_type,
                                "device_token": user[0].device_token,
                                "app_version": user[0].app_version,
                                "lng": user[0].lng,
                                "lat": user[0].lat
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].first_name;
                            func.sendMailTo(userName, email);
                        }

                    });
                }

            });
        });


    });
});

/*Login Panel*/

router.post('/login', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email, password];
    console.log(checkBlank);
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        },], function (updatePopup) {
            var sql = "SELECT `login_status` FROM `tb_user` WHERE `email`=? LIMIT 1"
            func.emailLogin(email, sql, 1, password, res);

        });
});

/*accessLogin Panel*/

router.post('/access_login', function (req, res, next) {
    var accessLogin = req.body.access_token;

    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, accessLogin, callback);
        },], function (updatePopup) {
            var sql = "SELECT `first_name`,`phone_no`,`email`,`device_token`,`device_type`,`app_version`,`lng`,`lat` FROM `tb_user` WHERE `access_token`=? LIMIT 1"
            func.accessTokenLogin(accessLogin, sql, 1, res);

        });
});

/*Logout Panel*/


router.post('/logout', function (req, res, next) {

    var accessToken = req.body.access_token;

    var sql = "SELECT `login_status` FROM `tb_user` WHERE `access_token`=? LIMIT 1"
    connection.query(sql, [accessToken], function (err, results) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (results.length === 0) {
            sendResponse.invalidAccessToken(res);
        }
        else if (results[0].login_status == 1) {
            var sql = "UPDATE `tb_user` SET `login_status`=? WHERE `access_token`=? LIMIT 1"
            connection.query(sql, [0, accessToken], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    sendResponse.sendSuccessLogout(res);
                }
            });
        }

    });
});

/*Edit Profile Panel*/

router.post('/edit', function (req, res, next) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var phoneNo = req.body.phone_no;
    var email = req.body.email;
    var accessToken = req.body.access_token;

    var checkBlank = [firstName, lastName, phoneNo, email, accessToken];
    console.log(req.body);
    async.waterfall([
        function (callback) {

            func.checkBlankWithCallback(res, checkBlank, callback);

        },
        function (callback) {
            var sql = "SELECT `user_id`, `access_token` FROM `tb_user` WHERE `email`=? LIMIT 1 ";

            func.checkEmail(res, sql, email, accessToken, callback);

        }], function (updatePopup) {

        func.uploadImage(req.files.user_image, 'jashan', function (result) {
            var sql = "UPDATE `tb_user` SET `email`=?,`first_name`=?,`last_name`=?,`image`=?,`phone_no`=? \n\
                     WHERE `access_token`=? ";
            connection.query(sql, [email, firstName, lastName, result, phoneNo, accessToken], function (err, result) {

                if (err) {

                    sendResponse.somethingWentWrongError(res);
                }
                else if (result.length == 0) {
                    sendResponse.invalidAccessToken(res);
                }
                else {
                    var sql = "SELECT `first_name`,`last_name`,`email`,`phone_no`,`user_id` ,`device_type`,`device_token`,`app_version`,`lat`,`lng` FROM \n\
                       `tb_user` WHERE `access_token`=? LIMIT 1";
                    connection.query(sql, [accessToken], function (err, user) {
                        if (err) {

                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var final = {
                                "first_name": user[0].first_name,
                                "last_name": user[0].last_name,
                                "access_token": user[0].access_token,
                                "email": user[0].email,
                                "phone_no": user[0].phone_no,
                                "device_type": user[0].device_type,
                                "device_token": user[0].device_token,
                                "app_version": user[0].app_version,
                                "lng": user[0].lng,
                                "lat": user[0].lat
                            };
                            sendResponse.editSuccessful(final, res);

                        }

                    });
                }

            });
        });


    });

});

/* Forgot Password Panel */

router.post('/forgot', function (req, res, next) {
    var email = req.body.email;
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, email, callback);
        },
            function (callback) {
                var sql = "SELECT `user_id` FROM `tb_user` WHERE `email`=? limit 1";
                func.checkEmailExist(res, sql, email, callback);

            }], function (updatePopup) {
            var sql = "SELECT `first_name` FROM `tb_user` WHERE `email`=? LIMIT 1";
            connection.query(sql, [email], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var userName = result[0].first_name;
                    func.sendMailToResetPassword(userName, 1, email, res);
                }
            });


        });
});

/* Reset Password Panel */

router.post('/reset', function (req, res, next) {
    var link = req.body.link;
    var newPassword = req.body.newpass;
    var checkData = [link, newPassword];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkData, callback);
        }], function (updatePopup) {
        var sql = "SELECT `user_id` FROM `tb_user` WHERE `temporary_link`=? LIMIT 1 "
        connection.query(sql, [link], function (err, result) {
            if (result.length == 0) {
                sendResponse.wrongLink(res);
            }
            else if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {

                var sql = "SELECT `user_id` FROM `tb_user` WHERE `temporary_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var md5 = require('MD5');
                        var hash = md5(newPassword);
                        var sql = "UPDATE `tb_user` SET `temporary_link`=? , `password`=? WHERE `user_id`=? LIMIT 1"
                        connection.query(sql, ["", hash, result[0].user_id], function (err, result) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                sendResponse.resetSuccessful(res);
                            }
                        })

                    }


                });
            }
        });
    });

});

/*Request Drivers*/

router.post('/request_drivers', function (req, res) {
    var accessToken = req.body.access_token;
    var lng = req.body.lng;
    var lat = req.body.lat;
    var scope = req.body.scope;
    checkData = [accessToken, lng, lat, scope];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkData, callback);
        }], function (updatePopup) {
        var sql = "SELECT `user_id` FROM `tb_user` WHERE `access_token`=?";
        connection.query(sql, [accessToken], function (err, result1) {
            if (err) {

                sendResponse.somethingWentWrongError(res);
            }
            else if (result1.length == 0) {
                sendResponse.invalidAccessToken(res);
            }
            else {
                var sql = "SELECT `driver_id`,`lat`,`lng` FROM `tb_driver` WHERE `verified`=?";
                connection.query(sql, [1], function (err, result) {
                    console.log(result);
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else if (result.length == 0) {
                        sendResponse.noDriver(res);
                    }
                    else {
                        var final = [];
                        var finalArray = [];
                        for (var i = 0; i < result.length; i++) {
                            var status = geolib.isPointInCircle(
                                {latitude: lat, longitude: lng},
                                {latitude: result[i].lat, longitude: result[i].lng},
                                scope
                            );

                            if (status) {
                                final.push([result[i].lat, result[i].lng]);
                                var sort = geolib.orderByDistance({latitude: lat, longitude: lng}, final);
                                finalArray.push([sort[i], result[i].driver_id]);

                            }

                        }
                        if (final.length == 0) {
                            sendResponse.noDriverAvailable(res);
                        }
                        else {
                            var sql = "INSERT INTO `tb_orders` (order_customer,order_lat,order_lng) VALUES(?,?,?)";
                            connection.query(sql, [result1[0].user_id, lat, lng], function (err, result) {
                                if (err) {
                                    sendResponse.somethingWentWrongError(res);
                                }
                                else {
                                    var final = {
                                        "user_id": result1[0].user_id,
                                        "array": sort
                                    };
                                    sendResponse.sendDriverData(final, res);
                                }
                            });

                            //pushNotifications.sendPush(sort1,res,constant.responseStatus.NEW_REQUEST, constant.responseStatus.NEW_REQUEST, function(err,callback) {
                            //    if (callback == 0)
                            //    {
                            //        console.log("Push Not Sent");
                            //    }
                            //    else
                            //    {
                            //        console.log("Push Sent");
                            //
                            //    }
                            //
                            //    sendResponse.sendDriverData(sort, res);
                            //});

                        }


                    }
                });

            }
        });


    });

});

/* Driver Request Accept*/

router.post('/driver_accept', function (req, res) {
    var accessToken = req.body.access_token;
    var user_id = req.body.user_id;
    var lat = req.body.lat;
    var lng = req.body.lng;
    var checkBlank = [accessToken, user_id, lat, lng];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        }
    ], function (updatePopup) {
        var sql = "SELECT `driver_id` FROM `tb_driver` WHERE `access_token`=? LIMIT 1";
        connection.query(sql, [accessToken], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.invalidAccessToken(res);
            }
            else {
                var sql = "SELECT `order_status` FROM `tb_orders` WHERE `order_customer`=? and `order_lat`=? and `order_lng`=? and `pickup_time` =? LIMIT 1";
                connection.query(sql, [user_id, lat, lng, "0000-00-00 00:00:00"], function (err, result1) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else if (result1.length == 0) {
                        sendResponse.driverAlreadyAssigned(res);
                    }
                    else if (result1[0].order_status == 1) {
                        sendResponse.driverAlreadyAssigned(res);
                    }
                    else if (result1[0].order_status == 0) {
                        console.log(result[0].driver_id);
                        var sql = "UPDATE `tb_orders` SET `order_driver`=? , `order_status`=? WHERE `order_customer`=? LIMIT 1"
                        connection.query(sql, [result[0].driver_id, 1, user_id], function (err, response) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                console.log(response);
                                var sql = "SELECT `first_name`,`last_name`,`phone_no`,`image` FROM `tb_driver` WHERE `driver_id`=? LIMIT 1";
                                connection.query(sql, [result[0].driver_id], function (err, result) {
                                    if (err) {
                                        sendResponse.somethingWentWrongError(res);
                                    }
                                    else {
                                        var final = {
                                            "first_name": result[0].first_name,
                                            "last_name": result[0].last_name,
                                            "phone_no": result[0].phone_no,
                                            "user_image": result[0].image
                                        };
                                        sendResponse.driverSuccessfullyAssigned(final, res);
                                    }
                                });
                            }

                        });
                    }
                });

            }


        });
    });

});

/*Start Ride*/

router.post('/start_ride', function (req, res) {
    var id = req.body.order_id;
    var lat = req.body.lat;
    var lng = req.body.lng;
    var time = new Date();
    checkBlank = [id, lat, lng];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        }
    ], function (updatePopup) {
        var sql = "SELECT `order_status` FROM `tb_orders` WHERE `order_id`=? and `pickup_time`=? LIMIT 1"
        connection.query(sql, [id, '0000-00-00 00:00:00'], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.invalidRide(res);
            }
            else {
                var sql = "UPDATE `tb_orders` SET `pickup_time`=?,`pickup_lat`=?,`pickup_lng`=? WHERE `order_id`=? LIMIT 1";
                connection.query(sql, [time, lat, lng, id], function (err, result) {
                    console.log(result);
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {
                            "pickup_time": time,
                            "pickup_lat": lat,
                            "pickup_lng": lng
                        };
                        sendResponse.rideStarted(data, res);
                    }
                });
            }
        });


    });
});

/*End Ride*/

router.post('/end_ride', function (req, res) {
    var id = req.body.order_id;
    var lat = req.body.lat;
    var lng = req.body.lng;
    var time = new Date();
    checkBlank = [id, lat, lng];
    console.log(checkBlank);
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        }
    ], function (updatePopup) {

        var sql = "SELECT `order_status` FROM `tb_orders` WHERE `order_id`=? and `pickup_time`!= ? and `drop_time`=? LIMIT 1"
        connection.query(sql, [id, '0000-00-00 00:00:00', '0000-00-00 00:00:00'], function (err, result) {
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.invalidRide(res);
            }
            else {
                var sql = "UPDATE `tb_orders` SET `drop_time`=?,`drop_lat`=?,`drop_lng`=? WHERE `order_id`=? LIMIT 1";
                connection.query(sql, [time, lat, lng, id], function (err, result) {
                    console.log(result);
                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {
                            "pickup_time": time,
                            "pickup_lat": lat,
                            "pickup_lng": lng
                        };
                        sendResponse.rideEnded(data, res);
                    }
                });
            }
        });


    });
});

/*search Drivers*/

router.post('/search_drivers', function (req, res) {
    var lng = req.body.lng;
    var lat = req.body.lat;
    var scope = req.body.scope;
    checkData = [lng, lat, scope];
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkData, callback);
        }], function (updatePopup) {

        var sql = "SELECT `verified`,`lat`,`lng` FROM `tb_driver`  WHERE `verified`=?";
        connection.query(sql, [1], function (err, result) {
            console.log(result);
            if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else if (result.length == 0) {
                sendResponse.noDriver(res);
            }
            else {
                var final = [];
                for (var i = 0; i < result.length; i++) {
                    var status = geolib.isPointInCircle(
                        {latitude: lat, longitude: lng},
                        {latitude: result[i].lat, longitude: result[i].lng},
                        scope
                    );

                    if (status) {
                        final.push([result[i].lat, result[i].lng]);
                        var sort = geolib.orderByDistance({latitude: lat, longitude: lng}, final);

                    }

                }
                if (final.length == 0) {
                    sendResponse.noDriverAvailable(res);
                }
                else {

                    sendResponse.sendDriverData(sort, res);
                }


            }
        });

    });

});


/*Driver Sign_Up Panel */

router.post('/driver_sign_up', function (req, res, next) {
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var phoneNo = req.body.phone_no;
    var email = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type;
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var lat = req.body.lat;
    var lng = req.body.lng;
    var checkBlank = [firstName, lastName, phoneNo, email, password, deviceType, deviceToken, appVersion, lat, lng];
    console.log(checkBlank);
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);

        },
        function (callback) {
            var sql = "SELECT `driver_id` FROM `tb_driver` WHERE `email`=? limit 1";
            func.checkEmailAvailability(res, sql, email, callback);

        }], function (updatePopup) {

        var hash = md5(password);
        var accesstoken = func.generateRandomString();
        var accesstoken1 = accesstoken + email;
        var accessToken = md5(accesstoken1);
        var loginTime = new Date();
        func.uploadImage(req.files.user_image, res, 'jashan', function (result) {
            var sql = "INSERT into tb_driver(email,password,access_token,date_registered,last_login,first_name,last_name,image,phone_no,device_type,device_token,app_version,lat,lng) \n\
                    values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            connection.query(sql, [email, hash, accessToken, loginTime, loginTime, firstName, lastName, result, phoneNo, deviceType, deviceToken, appVersion, lat, lng], function (err, result) {

                if (err) {
                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }

                else {
                    var sql = "SELECT `first_name`,`last_name`,`access_token`,`phone_no`,`driver_id` ,`device_type`,`device_token`,`app_version`,`lat`,`lng` FROM \n\
                       `tb_driver` WHERE `email`=? LIMIT 1";
                    connection.query(sql, [email], function (err, user) {
                        if (err) {
                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var final = {
                                "first_name": user[0].first_name,
                                "last_name": user[0].last_name,
                                "access_token": user[0].access_token,
                                "email": email,
                                "phone_no": user[0].phone_no,
                                "device_type": user[0].device_type,
                                "device_token": user[0].device_token,
                                "app_version": user[0].app_version,
                                "lng": user[0].lng,
                                "lat": user[0].lat
                            };
                            sendResponse.sendSuccessData(final, res);
                            var userName = user[0].first_name;
                            func.sendMailTo(userName, email);
                        }

                    });
                }

            });
        });


    });
});

/*Driver Login Panel*/

router.post('/driver_login', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email, password];
    console.log(checkBlank);
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        },], function (updatePopup) {
            var sql = "SELECT `verified` FROM `tb_driver` WHERE `email`=? LIMIT 1";
            connection.query(sql, [email], function (err, response) {
                if (err) {
                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }
                else if (response[0].verified == 0) {
                    sendResponse.notVerified(res);
                }
                else {
                    var sql = "SELECT `login_status` FROM `tb_driver` WHERE `email`=? LIMIT 1"
                    func.emailLogin(email, sql, 0, password, res);
                }
            });


        });
});

/*Driver accessLogin Panel*/

router.post('/driver_access_login', function (req, res, next) {
    var accessLogin = req.body.access_token;
    console.log(accessLogin);
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, accessLogin, callback);
        },], function (updatePopup) {
            var sql = "SELECT `first_name`,`phone_no`,`email`,`device_token`,`device_type`,`app_version`,`lng`,`lat` FROM `tb_driver` WHERE `access_token`=? LIMIT 1"
            func.accessTokenLogin(accessLogin, sql, 0, res);

        });
});

/*Driver Edit Profile Panel*/

router.post('/driver_edit', function (req, res, next) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var phoneNo = req.body.phone_no;
    var email = req.body.email;
    var accessToken = req.body.access_token;

    var checkBlank = [firstName, lastName, phoneNo, email, accessToken];
    console.log(req.body);
    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkBlank, callback);
        },
        function (callback) {
            var sql = "SELECT `driver_id`, `access_token` FROM `tb_driver` WHERE `email`=? LIMIT 1 ";
            func.checkEmail(res, sql, email, accessToken, callback);

        }], function (updatePopup) {

        func.uploadImage(req.files.user_image, 'jashan', function (result) {
            var sql = "UPDATE `tb_driver` SET `email`=?,`first_name`=?,`last_name`=?,`image`=?,`phone_no`=? \n\
                     WHERE `access_token`=? ";
            connection.query(sql, [email, firstName, lastName, result, phoneNo, accessToken], function (err, result) {

                if (err) {
                    console.log(err);
                    sendResponse.somethingWentWrongError(res);
                }
                else if (result.length == 0) {
                    sendResponse.invalidAccessToken(res);
                }
                else {
                    var sql = "SELECT `first_name`,`last_name`,`email`,`phone_no`,`driver_id` ,`device_type`,`device_token`,`app_version`,`lat`,`lng` FROM \n\
                       `tb_driver` WHERE `access_token`=? LIMIT 1";
                    connection.query(sql, [accessToken], function (err, user) {
                        if (err) {

                            sendResponse.somethingWentWrongError(res);
                        }
                        else {
                            var final = {
                                "first_name": user[0].first_name,
                                "last_name": user[0].last_name,
                                "access_token": user[0].access_token,
                                "email": user[0].email,
                                "phone_no": user[0].phone_no,
                                "device_type": user[0].device_type,
                                "device_token": user[0].device_token,
                                "app_version": user[0].app_version,
                                "lng": user[0].lng,
                                "lat": user[0].lat
                            };
                            sendResponse.editSuccessful(final, res);

                        }

                    });
                }

            });
        });


    });

});

/* Driver Forgot Password Panel */

router.post('/driver_forgot', function (req, res, next) {

    var email = req.body.email;
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, email, callback);
        },
            function (callback) {

                var sql = "SELECT `driver_id` FROM `tb_driver` WHERE `email`=? limit 1";
                func.checkEmailExist(res, sql, email, callback);

            }], function (updatePopup) {
            var sql = "SELECT `first_name` FROM `tb_driver` WHERE `email`=? LIMIT 1";
            connection.query(sql, [email], function (err, result) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    var userName = result[0].first_name;
                    func.sendMailToResetPassword(userName, 0, email, res);
                }
            });


        });
});

/*  Driver  Reset Password Panel */

router.post('/driver_reset', function (req, res, next) {
    var link = req.body.link;
    var newPassword = req.body.newpass;
    var checkData = [link, newPassword];

    async.waterfall([
        function (callback) {
            func.checkBlankWithCallback(res, checkData, callback);
        }], function (updatePopup) {
        var sql = "SELECT `driver_id` FROM `tb_driver` WHERE `temporary_link`=? LIMIT 1 "
        connection.query(sql, [link], function (err, result) {
            if (result.length == 0) {
                sendResponse.wrongLink(res);
            }
            else if (err) {
                sendResponse.somethingWentWrongError(res);
            }
            else {

                var sql = "SELECT `driver_id` FROM `tb_driver` WHERE `temporary_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var md5 = require('MD5');
                        var hash = md5(newPassword);
                        var sql = "UPDATE `tb_driver` SET `temporary_link`=? , `password`=? WHERE `driver_id`=? LIMIT 1"
                        connection.query(sql, ["", hash, result[0].driver_id], function (err, result) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                sendResponse.resetSuccessful(res);
                            }
                        })

                    }


                });
            }
        });
    });

});


module.exports = router;

