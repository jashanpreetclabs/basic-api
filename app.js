process.env.NODE_ENV = 'localDevelopment';
express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');
var app = express();
var multipart = require('connect-multiparty');
var routes = require('./routes/index');
var users = require('./routes/users');
config = require('config');
var multipartMiddleware = multipart();
var connection = require('./routes/mySqlLib');
md5 = require('MD5');

func = require('./routes/commonFunction');
sendResponse = require('./routes/sendResponse');
constant = require('./routes/constant');
//pushNotifications=require('./routes/pushNotifications');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', routes);

//customer

app.post('/sign_up', multipartMiddleware);
app.post('/sign_up', users);
app.post('/login', users);
app.post('/access_login', users);
app.post('/logout', users);
app.post('/edit', multipartMiddleware);
app.post('/edit', users);
app.post('/forgot', users);
app.post('/reset', users);
app.post('/request_drivers', users);
app.post('/search_drivers', users);

//Drivers

app.post('/driver_sign_up', multipartMiddleware);
app.post('/driver_sign_up', users);
app.post('/driver_login', users);
app.post('/driver_access_login', users);
app.post('/driver_edit', multipartMiddleware);
app.post('/driver_edit', users);
app.post('/driver_reset', users);
app.post('/driver_forgot', users);
app.post('/driver_accept', users);
app.post('/end_ride', users);
app.post('/start_ride', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

module.exports = app;
